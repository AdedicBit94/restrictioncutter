##restrictionEnzyme cutter 

* **Name**:             Armin Dedic
* **St.Number**:        342615
* **Group**:            BFV3
* **Course**:           Web-based information systems 1
* **Assignment**:       restrictionEnzyme cutter

## Description ##
This web-based program will be using an input DNA from the user to produce fragments by cutting the 
DNA sequence with restrictionEnzymes. Using those fragments, the program will calculate some properties of 
each of the fragment. The results will then be shown to the user in a table on the web page.
 
## Results information ##
 * Calculated GC percentage of each produced fragment
 * Calculated molecular Weight of each produced fragment
 * Start location of each produced fragment
 * End location of each produced fragment
 * Length of each produced fragment
 * Download link to the produced fragments

## Usage ##
 * Enter DNA into the box on the web page 
 * Select the restrictionEnzyme(s) 
 * Press the submit button
 * View the results on the next page, by clicking the "See the results?" which presents a table
