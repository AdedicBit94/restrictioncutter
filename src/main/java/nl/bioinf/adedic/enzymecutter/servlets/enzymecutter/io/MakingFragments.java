/*
 *Copyright (c) 2018. Armin Dedic [a.dedic@st.hanze.nl]
 *All rights reserved
 */

package nl.bioinf.adedic.enzymecutter.servlets.enzymecutter.io;
import nl.bioinf.adedic.enzymecutter.servlets.enzymecutter.model.FragmentsCalculations;

/*
 * making the fragments from the given DNA sequence by cutting them using the selected enzymes.
 * @author Armin Dedic [a.dedic@st.hanze.nl]
 * @version 0.0.1
 * All rights reserved.
 */

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MakingFragments {
    private String inputDNA;
    private String[] selectedEnzymes;

    /**
     * @param inputDNA, given DNA by the user
     * @param selectedEnzymes, selected enzymes by the user
     */
    public MakingFragments(String inputDNA, String[] selectedEnzymes) throws IOException {
        this.inputDNA = inputDNA;
        this.selectedEnzymes = selectedEnzymes;
    }

    /**
     * This method stores the fragments which are produced by the digestDNA in a String[]. It then
     * iterates over the array and passes the fragments to the fragmentsCalculation class which will calculate the
     * properties of it (i.e. length, start/stop location, GC and MW)
     * @return fragObjects, which contains all the calculated Properties.
     */
    public ArrayList<FragmentsCalculations> calculatePropertiesOfFragments() {
        String[] fragments = null;
        ArrayList<FragmentsCalculations> fragObjects = new ArrayList<>();
        fragments = digestDNA().split("#");

        for (String item : fragments){
            FragmentsCalculations FC = new FragmentsCalculations(item, inputDNA);
            fragObjects.add(FC);
        }
        return fragObjects;
    }

    /**
     * This method is used to digest the given DNA by enzymes using the replace method. By recognizing
     * the special character in the selected enzymes.
     * source: https://stackoverflow.com/questions/9605716/java-regular-expression-find-and-replace
     * @return producedFragments, are the fragments produced by digestion
     */
    private String digestDNA() {
        String producedFragments = "";
        for (String item : selectedEnzymes) {
            String pattern = item.replace("#", "");
            Pattern pattern1 = Pattern.compile(pattern);
            Matcher m = pattern1.matcher(inputDNA);
            while (m.find()) {
                producedFragments = inputDNA.replace(pattern, item);
                inputDNA = producedFragments;
            }
        }
        return producedFragments;
    }
}



