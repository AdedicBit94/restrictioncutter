/*
 *Copyright (c) 2018. Armin Dedic [a.dedic@st.hanze.nl]
 *All rights reserved
 */

package nl.bioinf.adedic.enzymecutter.servlets.enzymecutter.model;

/**
 * This class does the calculation of the fragments and will contain the properties of the fragments.
 * @author Armin Dedic [a.dedic@st.hanze.nl]
 * @version 0.0.1
 */

public class FragmentsCalculations {
    private String fragments;
    private final String inputDNA;

    /**
     * @param fragments, DNA pieces produced after cutting with selected enzymes.
     * @param inputDNA, DNA entered by the user.
     */
    public FragmentsCalculations(String fragments, String inputDNA) {
        this.fragments = fragments;
        this.inputDNA = inputDNA;
    }

    /**
     * This method is used as a getter to call en receive information.
     * @return GC percentage of the fragments
     */
    public Double getFragmentsGCCalculation(){
        gcPercentage gc = new gcPercentage();
        return gc.gcCalculation(fragments);
    }

    /**
     * This method is used as a getter to call en receive information.
     * @return molecularWeight of the fragments
     */
    public Double getFragmentsMolecularWeights(){
        MolecularWeight mw = new MolecularWeight();
        return mw.calculateWeights(fragments);
    }


    /**
     * This method is used to calculate the start location of the fragments
     * @return start location of the fragments
     */
    public int getFragmentsStartLocation(){
        String DNA = inputDNA.replace("#", "");
        int startFrag = DNA.indexOf(fragments);
        return startFrag + 1;
    }

    /**
     * This method is used to calculate the end location of the fragments
     * @return end location of the fragments
     */
    public int getFragmentsEndLocation(){
        String DNA = inputDNA.replace("#", "");
        int endFrag = DNA.lastIndexOf(fragments);
        return endFrag + (fragments.length() + 1);
    }

    /**
     * Retrieves the produced fragments
     * @return fragments
     */
    public String getFragments() {
        return fragments;
    }

    /**
     * This method gets the lengths of the fragments.
     * @return lengths of the fragments
     */
    public int getFragmentsLength(){
        return fragments.length();
    }










}
