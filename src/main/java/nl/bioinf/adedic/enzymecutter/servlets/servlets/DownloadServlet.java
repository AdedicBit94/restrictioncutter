package nl.bioinf.adedic.enzymecutter.servlets.servlets;

import nl.bioinf.adedic.enzymecutter.servlets.enzymecutter.io.DownloadFragments;
import nl.bioinf.adedic.enzymecutter.servlets.enzymecutter.model.FragmentsCalculations;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.ArrayList;

@WebServlet(name = "DownloadServlet", urlPatterns = "/downloadFasta")
public class DownloadServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Get current session
        HttpSession session = request.getSession();

        // Get fragments from current session
        ArrayList<FragmentsCalculations> fragmentProperties = (ArrayList<FragmentsCalculations>) session.getAttribute("fragments");

        //Determining the fragment numbers with the associated sequence of that fragment
        int fragNumber = Integer.parseInt(request.getParameter("fragment"));
        FragmentsCalculations writtenSeq = fragmentProperties.get(fragNumber - 1);
        DownloadFragments df = new DownloadFragments();
        File downloadFile = df.makeDownloadFragments(writtenSeq, fragNumber);

        // Forces download
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
        response.setHeader(headerKey, headerValue);

        // Creating a download content
        OutputStream outputStream = response.getOutputStream();
        FileInputStream inputStream = new FileInputStream(downloadFile);
        byte[] buffer = new byte[4096];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, length);
        }

        // Cleanup
        inputStream.close();
        outputStream.flush();
        downloadFile.delete();
    }
}
