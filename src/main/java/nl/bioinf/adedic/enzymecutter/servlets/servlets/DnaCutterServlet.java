package nl.bioinf.adedic.enzymecutter.servlets.servlets;

import nl.bioinf.adedic.enzymecutter.servlets.enzymecutter.io.DnaParser;
import nl.bioinf.adedic.enzymecutter.servlets.enzymecutter.model.FragmentsCalculations;
import nl.bioinf.adedic.enzymecutter.servlets.enzymecutter.io.MakingFragments;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "DnaCutterServlet", urlPatterns = "/home")
public class DnaCutterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String inputDNA = request.getParameter("inputDNA");
        String[] selectedEnzymes = request.getParameterValues("selectedEnzymes");

        // Parsing the entered DNA sequence to create fragments
        String sequence = parse_inputDNA(inputDNA);
        ArrayList<FragmentsCalculations> madeFragments = makeFragments(sequence, selectedEnzymes);

        HttpSession session = request.getSession();
        session.setAttribute("fragments", madeFragments);

        RequestDispatcher view = request.getRequestDispatcher("results.jsp");
        view.forward(request, response);
    }

    /**
     * Making fragment from the entered DNA sequence by using restriction enzymes that are selected
     * by the user.
     * @param sequence parsed sequence
     * @param selectedEnzymes selected enzymes
     * @return madeFragments
     * @throws IOException
     */
    private ArrayList<FragmentsCalculations> makeFragments(String sequence, String[] selectedEnzymes) throws IOException {
        MakingFragments fragments = new MakingFragments(sequence, selectedEnzymes);
        return fragments.calculatePropertiesOfFragments();
    }

    /**
     * Parsing the DNA that was entered by the user
     * @param inputDNA entered DNA sequence
     * @return parsed DNA sequence
     * @throws IOException
     */
    private String parse_inputDNA(String inputDNA) throws IOException {
        DnaParser parser = new DnaParser(inputDNA);
        return parser.parseHeader();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
        dispatcher.forward(request, response);
    }
}
