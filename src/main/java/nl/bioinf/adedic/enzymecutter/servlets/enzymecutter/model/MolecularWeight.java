/*
 *Copyright (c) 2018. Armin Dedic [a.dedic@st.hanze.nl]
 *All rights reserved
 */

package nl.bioinf.adedic.enzymecutter.servlets.enzymecutter.model;

import java.util.HashMap;
import java.util.Map;

/**
 * This class calculates the molecular weight of the produced fragments.
 * @author Armin Dedic [a.dedic@st.hanze.nl]
 * @version 0.0.1
 */
class MolecularWeight {
    private static final Map<Character, Double> dnaWeights;

    /*
      HashMap that contains values for each DNA base which can be used
      to calculate molecular weight of each fragment.
     */
    static {
        dnaWeights = new HashMap<>();
        dnaWeights.put('A', 135.13);
        dnaWeights.put('T', 126.12);
        dnaWeights.put('G', 151.13);
        dnaWeights.put('C', 111.10);
    }

    /**
     * Calculating the molecular weight of the produced fragments
     * @param fragments, produced fragments from DNA sequence
     * @return total molecular weight of fragment
     */
    Double calculateWeights(String fragments) {
        Double total = 0.0;
        for (Character dna : fragments.toCharArray()) {
            total += dnaWeights.get(dna);
        }
        return (double) Math.round(total * 100) / 100;
    }
}
