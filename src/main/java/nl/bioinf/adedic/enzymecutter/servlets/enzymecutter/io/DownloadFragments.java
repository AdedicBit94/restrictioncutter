/*
 *Copyright (c) 2018. Armin Dedic [a.dedic@st.hanze.nl]
 *All rights reserved
 */
package nl.bioinf.adedic.enzymecutter.servlets.enzymecutter.io;

import nl.bioinf.adedic.enzymecutter.servlets.enzymecutter.model.FragmentsCalculations;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;

/**
 * This class writes the sequence of the fragments with the corresponding header to the download file
 * @author Armin Dedic [a.dedic@st.hanze.nl]
 * @version 0.0.1
 */
public class DownloadFragments {
    /**
     * @param fragmentProperties
     * @param fragNumber
     * @return file
     */
    public File makeDownloadFragments(FragmentsCalculations fragmentProperties, int fragNumber) {
        String sequence = fragmentProperties.getFragments();
        String filename = MessageFormat.format("fragment_{0}.fasta", fragNumber);
        File file = new File(filename);
        String newHeader = MessageFormat.format(">Fragment:{0}", fragNumber);

        // line separator to paste the sequence below the header.
        String newline = System.getProperty("line.separator");

        // Writing to the download file
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(newHeader);
            writer.write(newline + sequence);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
}
