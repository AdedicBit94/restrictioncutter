/*
 *Copyright (c) 2018. Armin Dedic [a.dedic@st.hanze.nl]
 *All rights reserved
 */

package nl.bioinf.adedic.enzymecutter.servlets.enzymecutter.model;

/**
 * This class calculated the GC-percentage of the produced fragments.
 * @author Armin Dedic [a.dedic@st.hanze.nl]
 * @version 0.0.1
 */
class gcPercentage {
    /**
     * This method calculates the GC-percentage of each fragment that is produced.
     * @param fragments, DNA pieces produced after cutting
     * @return gcPercentage
     */
    Double gcCalculation(String fragments){
        double gcPercentage = 0;
        int Len = fragments.length();
        double gcCount = 0;
        for (int i = 0; i < Len; i++){
            if (fragments.charAt(i)== 'G' || fragments.charAt(i)== 'C') gcCount++;
        }
        gcPercentage += (gcCount/Len * 100);
        return (double) Math.round(gcPercentage * 100) / 100;
    }
}
