/*
 *Copyright (c) 2018. Armin Dedic [a.dedic@st.hanze.nl]
 *All rights reserved
 */

package nl.bioinf.adedic.enzymecutter.servlets.enzymecutter.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

/**
 * This class parses the given DNA sequence by skipping the header and resturns the sequence
 * in upperCase form.
 * @author Armin Dedic [a.dedic@st.hanze.nl]
 * @version 0.0.1
 */
public class DnaParser {
    private String DNA;

    public DnaParser(String DNA) {
        this.DNA = DNA;
    }

    /**
     * Reads the given DNA and parses its sequence to skip the header. Changes the nucleotides
     * bases to uppercase.
     * @return parsed sequence
     * @throws IOException
     */
    public String parseHeader() throws IOException {
        BufferedReader reader = new BufferedReader(new StringReader(DNA));
        StringBuilder sequence = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            if (!line.startsWith(">")) {
                sequence.append(line);
            }
        }
        return sequence.toString().toUpperCase();
    }
}
