function ErrorMessage(text){
    $("#error-msg").empty().text(text).fadeIn().delay(5000).fadeOut();
}

$(document).ready(function () {

    $('#form').submit(function(e) {
        e.preventDefault();
        if (validateFasta()){
            this.submit();
        }
    });

    function validateFasta() {
        var DNA = $.trim($("#inputDNA").val());

        // Check for selection of at least one enzyme
        if(!$("input[type=checkbox]:checked").length) {
            ErrorMessage("you must check at least one checkbox");
            return false;
        }

        if(DNA.indexOf(">") === 0){
            if(!DNA.match("\n")){
                ErrorMessage("Between header and sequence need to be an enter please try again");
                return false;
            }else {
                DNA = DNA.substring(DNA.indexOf("\n")+1);
                console.log(DNA);
            }
        }

        // Check for valid sequence
        if(!/^[AGCT]+$/i.test(DNA)){
            ErrorMessage("No valid DNA (may only contain ACGT)");
            return false;
        }

        return true;
    }
});

//source for dnaValid: http://www.dnacoil.com/tools/validate-dna-fasta-file-with-a-javascript-function/

