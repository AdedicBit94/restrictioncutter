<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Armin
  Date: 8-3-2018
  Time: 15:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Armin Dedic</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/DnaValidation.js"></script>
</head>
<body>
    <h1>Restriction enzyme cutter</h1>
    <p>Author: Armin Dedic</p>
    <p>Class:  BFV3</p>
    <p>Study:  Bio-informatica</p>
<div class="container">
    <h4>${requestScope.errorMsg}</h4>
    <div class="row">
        <h4><a href="#hidden" data-toggle="collapse">See the results?</a></h4>
        <div id="hidden" class="collapse">
            <c:choose>
                <c:when test="${fragments != null}">
                    <table class ="tabelclas">
                        <tr>
                            <th>Fragment</th>
                            <th>GC-percentage</th>
                            <th>Moleculair Weight</th>
                            <th>Start location</th>
                            <th>Stop location</th>
                            <th>Fragment length</th>
                            <th>Download</th>
                        </tr>
                        <c:forEach var="fragment" items="${fragments}" varStatus="home">
                            <tr>
                                <td>${home.count}                               </td>
                                <td>${fragment.getFragmentsGCCalculation()}     </td>
                                <td>${fragment.getFragmentsMolecularWeights()}  </td>
                                <td>${fragment.getFragmentsStartLocation()}     </td>
                                <td>${fragment.getFragmentsEndLocation()}       </td>
                                <td>${fragment.getFragmentsLength()}            </td>
                                <td><a href="<c:url value="/downloadFasta?fragment=${home.count}"/>">Download Fragments</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:when>
                <c:otherwise>
                    <c:redirect url="home.jsp"/>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
</body>
</html>
