<%--
  Created by IntelliJ IDEA.
  User: Armin
  Date: 4-2-2018
  Time: 09:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Armin Dedic</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">


</head>
    <h1>Restriction enzyme cutter</h1>
    <p>Author: Armin Dedic</p>
    <p>Class:  BFV3</p>
    <p>Study:  Bio-informatica</p>
<body>
<div class="container">
    <div class="row">
        <h4><a href="#hidden" data-toggle="collapse">Would you like to try this program?</a></h4>
        <div id="hidden" class="collapse">
            <div class="Enzyme_cutter_form">

                <form name="form" id="form" action="home" method="post">
                    <div class="text-area">
                        <p><label>Enter DNA into the box</label></p>
                        <label>
                            <textarea id="inputDNA" name="inputDNA" required="required" cols="30" rows="4"></textarea>
                            <div id="error-msg"></div>
                        </label>
                    </div>
                    <div>
                        <p><label>Select enzymes and press Submit</label></p>
                            <label><input type="checkbox" name="selectedEnzymes" id="HindIII" value="A#AGCTT"> HindIII</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="AatII" value="GACGT#C"> AatII</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="AfeI" value="AGC#GCT"> AfeI</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="TaqI" value="T#CGA"> TaqI</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="BamHI" value="G#GATCC"> BamHI</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="AclI" value="AA#CGTT"> AclI</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="HpaI" value="GTT#AAC"> HpaI</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="EcoRV" value="GAT#ATC"> EcoRV</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="EcoRI" value="G#AATTC"> EcoRI</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="DraI" value="TTT#AAA"> DraI</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="ClaI" value="AT#CGAT"> ClaI</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="BstBI" value="TT#CGAA"> BstBI</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="BclI" value="T#GATCA"> BclI</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="Acc65I" value="G#GTACC"> Acc65I</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="BglII" value="A#GATCT"> BglII</label>
                            <label><input type="checkbox" name="selectedEnzymes" id="AgeI" value="A#CCGGT"> AgeI</label>
                    </div>
                    <button type="submit" id="checkBtn">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/DnaValidation.js"></script>

</body>
</html>
